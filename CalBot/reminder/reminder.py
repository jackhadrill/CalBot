import discord
import socket
from CalBot.discord import DiscordClient


class Reminder():

    def __init__(self, client):
        self._discord = client
        self._discord.add_command('!reminder', self._parse_command)

    def _parse_command(self, command):
        embed = discord.Embed(color=0x00ff00)
        embed.set_image(url='https://media.discordapp.net/attachments/506852356898422797/715690132883111996/Capture.PNG')
        return embed
