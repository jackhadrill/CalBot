#!/usr/bin/env python3

from setuptools import setup, find_packages

with open('LICENSE') as f:
    license = f.read()

setup(
    name='calbot',
    version='0.1',
    description='A Discord bot for Callum',
    author='Jack Hadrill',
    author_email='',
    url='https://git.jacknet.io/jackhadrill/CalBot',
    license=license,
    packages=[
        'CalBot',
        'CalBot.discord',
        'CalBot.reminder',
    ],
    entry_points={
        'console_scripts' : [
            'calbot = CalBot.main:main'
        ]
    },
    install_requires=[
        'discord.py>=1.3.1',
    ]
)
