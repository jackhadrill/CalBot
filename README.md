# CalBot

Use a Python 3.8 venv. Install using...
```bash
python38 -m venv venv
source venv/bin/activate
pip install .
```
  
Run using...
```
calbot
```